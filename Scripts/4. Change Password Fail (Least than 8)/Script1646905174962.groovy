import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.junit.Assert as Assert
WebUI.openBrowser('')

WebUI.navigateToUrl('https://myid.buu.ac.th/')

WebUI.setText(findTestObject('Change_Fail_Leastthan8/Page_My ID/input_(Username)_user'), '62160247')

WebUI.setEncryptedText(findTestObject('Change_Fail_Leastthan8/Page_My ID/input_(Password)_pass'), '0Vqzwkd/z7b6GJsAbCMgBQ==')

WebUI.click(findTestObject('Change_Fail_Leastthan8/Page_My ID/button_Sign in'))

WebUI.click(findTestObject('Change_Fail_Leastthan8/Page_My ID/a_Change PasswordRenew password'))

WebUI.setEncryptedText(findTestObject('Change_Fail_Leastthan8/Page_My ID/input_(New Password)_newpass'), 
    'iaouGaigjoc=')

WebUI.setEncryptedText(findTestObject('Change_Fail_Leastthan8/Page_My ID/input_(Re-New Password)_renewpass'), 
    'iaouGaigjoc=')

WebUI.click(findTestObject('Change_Fail_Leastthan8/Page_My ID/button_Change Password'))

actaulResult = WebUI.getText(findTestObject('Change_Fail_Leastthan8/Page_My ID/div_8   25 Password more than 8 - 25 character'))
expectedResult = 'รหัสผ่านของท่าน\nต้องมีความยาวขั้นต่ำ 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร\nPassword more than 8 - 25 character'
Assert.assertEquals(expectedResult, actaulResult)
WebUI.closeBrowser()

